//
//  SharedObject.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import CoreLocation

class SharedObject {
    
    static let providerService = ProviderService()
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static var locationManager: CLLocationManager?
    static var userLocation : CLLocation?
    static var favourateLocationsList: [FavourateLocation] = [FavourateLocation]()
    static var choosenDeliveryLocation: FavourateLocation?
    
    static let layout = UICollectionViewFlowLayout()
    
    static var providerViewController = UINavigationController(rootViewController: ProviderViewController())
    static var orderCollectionViewController = UINavigationController(rootViewController:OrderCollectionViewController(collectionViewLayout: layout))
    static var basketCollectionViewController = UINavigationController(rootViewController:BasketViewController())
    static var offerCollectionViewController = UINavigationController(rootViewController:OfferCollectionViewController(collectionViewLayout: layout))
    static var registerViewController = RegisterController()
    static var favourateLocationsController = FavourateLocationsController(collectionViewLayout: layout)
    
}
