//
//  SharedValues.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class SharedValues {
    
    static var favourateLocationsListKey: String = "favourateLocationsListKey"
//    static var savedFavourateLocationCountKey: String = "locationsCount"
    static let buttonShadowWhiteBackground = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.5)
    static let appPreferredTextColor = UIColor(red: 130, green: 33, blue: 17)
    static let defaultControlHeight: CGFloat = 35
    static let distanceBetweenControls: CGFloat = 25
    static let defaultPadding: CGFloat = 10
    static let currentLocationDescreption: String = ""
    static var distanceFilter: Double = 1000
    
    class ProviderSearchResultController {
        static let providerCellId = "providerCellId"
        static let adDefaultImage: UIImage = UIImage(named: "pic2")!
    }
    
    class LoginController {
        static let loginPromptText: String = "Login"
        static let loginWithFingerPrintPromptText: String = "Login With Fingerprint"
        static let rememberMePromptText: String = "Remember Me"
        static let providerType: String = "Provider"
        static let driverType: String = "Driver"
    }
    
    class RegisterController {
        static let savePromptText: String = "Save"
        static let cityPromptText: String = "City"
        static let registerPromptText: String = "Register / New Account"
        static let phonePromptText: String = "Phone"
        static let locationPromptText: String = "Location"
        static let confirmMagicPromptText: String = "Confirm Password"
        static let magicPromptText: String = "Password"
        static let emailPromptText: String = "Email"
        static let nicknamePromptText: String = "Commercial Name"
        static let firstnamePromptText: String = "First Name"
        static let lastnamePromptText: String = "Last Name"
        static let driverSegmentTitle: String = "Driver"
        static let providerSegmentTitle: String = "Provider"
    }
    
    class ProviderController {
        static let tabbarIcon: UIImage! = UIImage(named: "family-Small")
        static let tabbarTitle: String = "Families"
        static let backgroundImage: UIImage! = UIImage(named: "backgroundImage")
        static let logoImage: UIImage! = UIImage(named: "logo_2")
        static let searchBackgroundColor: UIColor = UIColor.white
        static let locationButtonTitle: String = "Location"
        static let locationButtonTitleColor: UIColor = UIColor.black
        static let searchButtonTitle: String = "Search"
        static let searchButtonTitleColor: UIColor = UIColor.black
        static let navigationBarTitle: String = "Provider"
    }
    
    class OfferController {
        static let offerCellId = "offerCell"
        static let tabbarIcon: UIImage! = UIImage(named: "gift-Small")
        static let tabbarTitle: String = "Offers"
        static let navigationBarTitle: String = "Offers"
    }
    
    class OrderController {
        static let orderCellId = "orderCellId"
        static let tabbarIcon: UIImage! = UIImage(named: "shopping-Small")
        static let tabbarTitle: String = "Orders"
        static let navigationBarTitle: String = "Orders"
        class OrderCell{
            static let providerNameLabel: String = "Provider"
            static let providerAddressLabel: String = "Address"
            static let providerPhoneLabel: String = "Phone"
            static let deliveryStatusLabel: String = "Delivery Status"
            static let paymentMethodLabel: String = "Payment Method"
            static let quantityLabel: String = "Quantity"
            static let discountCostLabel: String = "Discount"
            static let taxCostLabel: String = "Tax"
            static let deliveryCostLabel: String = "Delivery"
            static let totalCostLabel: String = "Total"
        }
    }
    
    class BasketController {
        static let basketCellId = "basketCellId"
        static let tabbarIcon: UIImage! = UIImage(named: "bag-Small")
        static let tabbarTitle: String = "Basket"
        static let navigationBarTitle: String = "Basket"
        static let agreeButtonLabel: String = "Agree"
        static let disagreeButtonLabel: String = "Disgree"
        static let cashButtonLabel: String = "Cash"
        static let creditCardButtonLabel: String = "Credit"
        static let orderNowButtonLabel: String = "Order Now"
        static let chooseDateButtonLabel: String = "Choose Date"
    }
    
    class FavourateLocation {
        static let locationCellId: String = "locationCellId"
        static let locationFooterCellId: String = "locationFooterCellId"
        static let pickLocationPromptText: String = "Pick Location"
    }
    
    class MapController {
        static let savePromptText: String = "Save"
        static let pleaseEnterLocationName: String = "Please Enter Location Name"
        static let locationNamePromptText: String = "Location Name"
        static let useOnlyPromptText: String = "Use Only"
        static let cancelPromptText: String = "Cancel"
        static let selectPromptText: String = "Select"
        static let navigationBarTitle: String = "Pick Location"
    }
    
    class ProviderProductController {
        static let productCellId: String = "productCellId"
        static let categoryCellId: String = "categoryCellId"
        static let commentsPromptText: String = "Comments"
    }
    
    class CommentController {
        static let commentPromptText: String = "Comments"
        static let commentCellId: String = "commentCellId"
        static let rateCountPromptText: String = "Rate"
        static let rateNowPromptText: String = "Rate Now"
    }
    
    class ProviderAdminController {
        static let providerAdminCellId: String = "providerAdminCellId"
    }
    
    class DriverAdminController {
        static let driverAdminCellId: String = "driverAdminCellId"
    }
    
    class API {
        static let baseURL: String = "http://freehandes-001-site1.etempurl.com/:64281/api/Manager/"
        class MethodName {
            static let providerRegister: String = "Provider_Register"
            static let providerLogin: String = "Provider_Login"
            static let driverLogin: String = "Driver_Login"
            static let driverRegister: String = "Driver_Register"
        }
        class Parameters {
            static var token: String = ""
            static var lat: String = ""
            static var log: String = ""
        }
    }
    
}
