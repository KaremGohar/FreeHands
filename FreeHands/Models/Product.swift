//
//  Product.swift
//  FreeHands
//
//  Created by Apple on 7/17/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class Product {
    
    var id: String?
    var name: String?
    var image: UIImage?
    var price: Float?
    var description: String?
    var category: String?
    
}
