//
//  FavourateLocation.swift
//  FreeHands
//
//  Created by Apple on 7/6/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import  Foundation

class FavourateLocation: NSObject, NSCoding {
    
    var name: String?
    var addressName: String?
    var latitude: Double?
    var longtitude: Double?
    
    init(name: String, addressName: String, latitude: Double, longtitude: Double) {
        self.name = name
        self.addressName = addressName
        self.latitude = latitude
        self.longtitude = longtitude
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let addressName = aDecoder.decodeObject(forKey: "addressName") as! String
        let latitude = aDecoder.decodeObject(forKey: "latitude") as! Double
        let longtitude = aDecoder.decodeObject(forKey: "longtitude") as! Double
        self.init(name: name,addressName: addressName, latitude: latitude, longtitude: longtitude)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(name, forKey: "addressName")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longtitude, forKey: "longtitude")
    }
    
}
