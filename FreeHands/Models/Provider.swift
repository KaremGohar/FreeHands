//
//  Provider.swift
//  FreeHands
//
//  Created by Apple on 7/4/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import Foundation

class Provider: Codable{
    
    private enum CodingKeys : String, CodingKey {
        case Name = "Name"
        case FirstName = "FirstName"
        case LastName = "LastName"
        case TradeName = "TradeName"
        case Email = "Email"
        case Password = "Password"
        case Lat = "Lat"
        case Log = "Log"
        case Location = "Location"
        case Phone = "Phone"
        case KnownNumber = "KnownNumber"
        case Provider_Photo = "Provider_Photo"
        case Provider_Identity = "Provider_Identity"
    }
    
    var Name: String?
    var FirstName: String?
    var LastName: String?
    var TradeName: String?
    var Email: String?
    var Password: String?
    var Lat: String?
    var Log: String?
    var Location: String?
    var Phone: String?
    var KnownNumber: String?
    var Provider_Photo: String?
    var Provider_Identity: String?
    
}
