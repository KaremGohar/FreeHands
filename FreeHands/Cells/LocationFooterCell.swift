//
//  LocationFooterCell.swift
//  FreeHands
//
//  Created by Apple on 7/8/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class LocationFooterCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let cellButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.FavourateLocation.pickLocationPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: .right, underLine: false), for: .normal)
        return view
    }()
    
    let thumbImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "locationIcon-Small")
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension LocationFooterCell {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setOfferButtonViewConstraints(button: cellButton)
        setOfferImageViewConstraints(image: thumbImage)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(cellButton)
        addSubview(thumbImage)
    }
    
    private func setOfferButtonViewConstraints(button: UIButton){
        NSLayoutConstraint.activate([
            button.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            button.topAnchor.constraint(equalTo: self.topAnchor),
            button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
    }
    
    private func setOfferImageViewConstraints(image: UIImageView){
        NSLayoutConstraint.activate([
            image.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight/2),
            image.widthAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight/2),
            image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: SharedValues.defaultControlHeight/2),
            image.centerYAnchor.constraint(equalTo: self.centerYAnchor)
            ])
    }
    
}
