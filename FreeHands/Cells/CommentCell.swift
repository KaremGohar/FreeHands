//
//  CommentCell.swift
//  FreeHands
//
//  Created by Apple on 7/12/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import DJWStarRatingView

class CommentCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let cellTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    let cellSubTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .yellow
        
        return view
    }()
    
    let cellDescreption: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .cyan
        
        return view
    }()
    
    let rating : DJWStarRatingView = {
        let view = DJWStarRatingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.starSize = CGSize(width: 20, height: 20)
        view.numberOfStars = 5
        view.fillColor = .yellow
        view.strokeColor = .yellow
        view.unfilledColor = .clear
        view.rating = 2.5
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CommentCell {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setCellTitleLabelViewConstraints(control: cellTitle)
        setCellSubTitleLabelViewConstraints(control: cellSubTitle)
        setCellDescriptionTitleLabelViewConstraints(control: cellDescreption)
        setRatingTitleLabelViewConstraints(control: rating)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(cellTitle)
        addSubview(cellSubTitle)
        addSubview(cellDescreption)
        addSubview(rating)
    }
    
    private func setCellTitleLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: self.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setCellSubTitleLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: cellTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setCellDescriptionTitleLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: cellSubTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setRatingTitleLabelViewConstraints(control: DJWStarRatingView){
        NSLayoutConstraint.activate([
            control.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: SharedValues.defaultPadding),
            control.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: SharedValues.defaultPadding),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.30)
            ])
    }
    
}
