//
//  OfferCell.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class OfferCell: UICollectionViewCell {
    
    var currentObject : Offer? {
        didSet {
//            titleTextField.setTitle(currentObject?.title, for: .normal)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    private func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setOfferButtonViewConstraints(button: offerButton)
        setOfferImageViewConstraints(image: offerImage)
        setOfferNameLabelViewConstraints(label: offerLabel)
        setOfferPriceLabelViewConstraints(label: offerPrice)
        setProviderNameLabelViewConstraints(label: offerProviderName)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(offerImage)
        addSubview(offerLabel)
        addSubview(offerPrice)
        addSubview(offerProviderName)
        addSubview(offerButton)
    }
    
    private func setOfferButtonViewConstraints(button: UIButton){
        NSLayoutConstraint.activate([
            button.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            button.topAnchor.constraint(equalTo: self.topAnchor),
            button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
    }
    
    private func setOfferImageViewConstraints(image: UIImageView){
        NSLayoutConstraint.activate([
            image.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.5),
            image.topAnchor.constraint(equalTo: self.topAnchor),
            image.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
    }
    
    private func setOfferNameLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.centerYAnchor, constant: 10),
            label.leadingAnchor.constraint(equalTo: self.centerXAnchor),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            label.heightAnchor.constraint(equalToConstant: 15)
            ])
    }
    
    private func setProviderNameLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            label.leadingAnchor.constraint(equalTo: self.centerXAnchor),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            label.heightAnchor.constraint(equalToConstant: 15)
            ])
    }
    
    private func setOfferPriceLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.centerYAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 15)
            ])
    }
    
    let offerButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let offerPrice: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let offerProviderName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let offerLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let offerImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleToFill
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
