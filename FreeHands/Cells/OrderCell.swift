//
//  OrderCell.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class OrderCell: UICollectionViewCell {
    
    var currentObject : Order? {
        didSet {
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let providerNameLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.providerNameLabel
        return view
    }()
    
    let providerName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let providerAddressLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.providerAddressLabel
        return view
    }()
    
    let providerAddress: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let providerPhoneLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.providerPhoneLabel
        return view
    }()
    
    let providerPhone: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(SharedMethod.callNumber), for: .touchUpInside)
        return view
    }()
    
    let deliveryStatusLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.deliveryStatusLabel
        return view
    }()
    
    let deliveryStatus: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let paymentMethodLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.paymentMethodLabel
        return view
    }()
    
    let paymentMethod: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let quantityLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.quantityLabel
        return view
    }()
    
    let quantity: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let discountCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.discountCostLabel
        return view
    }()
    
    let discountCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let taxCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.taxCostLabel
        return view
    }()
    
    let taxCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let deliveryCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.deliveryCostLabel
        return view
    }()
    
    let deliveryCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let totalCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.totalCostLabel
        return view
    }()
    
    let totalCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let orderStatusProgressBar: UIProgressView = {
        let view = UIProgressView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension OrderCell {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setProviderNameLabelViewConstraints(label: providerNameLabel)
        setProviderNameViewConstraints(label: providerName)
        setProviderAddressLabelViewConstraints(label: providerAddressLabel)
        setProviderAddressViewConstraints(label: providerAddress)
        setProviderPhoneLabelViewConstraints(label: providerPhoneLabel)
        setProviderPhoneViewConstraints(label: providerPhone)
        setDeliveryStatusLabelLabelViewConstraints(label: deliveryStatusLabel)
        setDeliveryStatusViewConstraints(label: deliveryStatus)
        setPaymentMethodLabelViewConstraints(label: paymentMethodLabel)
        setPaymentMethodViewConstraints(label: paymentMethod)
        setQuantityLabelViewConstraints(label: quantityLabel)
        setQuantityViewConstraints(label: quantity)
        setDiscountLabelViewConstraints(label: discountCostLabel)
        setDiscountViewConstraints(label: discountCost)
        setTaxLabelViewConstraints(label: taxCostLabel)
        setTaxViewConstraints(label: taxCost)
        setDeliveryCostLabelViewConstraints(label: deliveryCostLabel)
        setDeliveryCostViewConstraints(label: deliveryCost)
        setTotalLabelViewConstraints(label: totalCostLabel)
        setTotalCostViewConstraints(label: totalCost)
        setOrderProgressViewConstraints(bar: orderStatusProgressBar)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(providerNameLabel)
        addSubview(providerName)
        addSubview(providerAddressLabel)
        addSubview(providerAddress)
        addSubview(providerPhoneLabel)
        addSubview(providerPhone)
        addSubview(deliveryStatusLabel)
        addSubview(deliveryStatus)
        addSubview(paymentMethodLabel)
        addSubview(paymentMethod)
        addSubview(quantityLabel)
        addSubview(quantity)
        addSubview(discountCostLabel)
        addSubview(discountCost)
        addSubview(taxCostLabel)
        addSubview(taxCost)
        addSubview(deliveryCostLabel)
        addSubview(deliveryCost)
        addSubview(totalCostLabel)
        addSubview(totalCost)
        addSubview(orderStatusProgressBar)
    }
    
    private func setProviderNameLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderNameViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: providerNameLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderAddressLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerNameLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderAddressViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerName.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: providerAddressLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderPhoneLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerAddressLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderPhoneViewConstraints(label: UIButton){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerAddress.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: providerPhoneLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDeliveryStatusLabelLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerPhoneLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDeliveryStatusViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerPhone.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: providerAddress.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setPaymentMethodLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryStatusLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setPaymentMethodViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryStatus.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: paymentMethodLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    
    
    private func setQuantityLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setQuantityViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: quantityLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDiscountLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: quantityLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDiscountViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: quantity.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: discountCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setTaxLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: discountCostLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setTaxViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: discountCost.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: taxCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDeliveryCostLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: taxCostLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDeliveryCostViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: taxCost.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: deliveryCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setTotalLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryCostLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setTotalCostViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryCost.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: totalCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    
    private func setOrderProgressViewConstraints(bar: UIProgressView){
        NSLayoutConstraint.activate([
            bar.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bar.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            bar.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bar.heightAnchor.constraint(equalToConstant: 10)
            ])
    }
    
}









