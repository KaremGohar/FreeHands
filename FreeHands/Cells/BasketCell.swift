//
//  BasketCell.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class BasketCell: UICollectionViewCell {
    
    var currentObject : BasketItem? {
        didSet {
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
//        setupPageSettings()
    }
    
    fileprivate func setupPageSettings(){
        createDatePicker()
    }
    
    private func createDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        chooseDateButton.inputAccessoryView = toolbar
        chooseDateButton.inputView = chooseDateButton
        expiryDatePickerView.datePickerMode = .dateAndTime
    }
    
    @objc private func donePressed() {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        let dateString = formatter.string(from: expiryDatePickerView.date)
        chooseDateButton.text = "\(dateString)"
        self.endEditing(true)
    }
    
    fileprivate let expiryDatePickerView: UIDatePicker = UIDatePicker()
    
    let providerNameLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.providerNameLabel
        return view
    }()
    
    let providerName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let providerAddressLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.providerAddressLabel
        return view
    }()
    
    let providerAddress: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let providerPhoneLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.providerPhoneLabel
        return view
    }()
    
    let providerPhone: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(SharedMethod.callNumber), for: .touchUpInside)
        return view
    }()
    
    let deliveryStatusLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.deliveryStatusLabel
        view.textAlignment = .center
        return view
    }()
    
    lazy var deliveryStatusStack: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .equalCentering
        view.alignment = .center
        view.spacing = 15
        view.addArrangedSubview(agreeOnDeliveryLabel)
        view.addArrangedSubview(agreeOnDeliveryButton)
        view.addArrangedSubview(disagreeOnDeliveryLabel)
        view.addArrangedSubview(disagreeOnDeliveryButton)
        return view
    }()
    
    let agreeOnDeliveryLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.BasketController.agreeButtonLabel
        return view
    }()
    
    let agreeOnDeliveryButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 20).isActive = true
        view.heightAnchor.constraint(equalToConstant: 20).isActive = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let disagreeOnDeliveryLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.BasketController.disagreeButtonLabel
        return view
    }()
    
    let disagreeOnDeliveryButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 20).isActive = true
        view.heightAnchor.constraint(equalToConstant: 20).isActive = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let paymentMethodLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.paymentMethodLabel
        view.textAlignment = .center
        return view
    }()
    
    lazy var paymentMethodStack: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .equalCentering
        view.alignment = .center
        view.spacing = 15
        view.addArrangedSubview(creditCardLabel)
        view.addArrangedSubview(creditCardButton)
        view.addArrangedSubview(cashLabel)
        view.addArrangedSubview(cashButton)
        return view
    }()
    
    let creditCardLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.BasketController.creditCardButtonLabel
        return view
    }()
    
    let creditCardButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 20).isActive = true
        view.heightAnchor.constraint(equalToConstant: 20).isActive = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let cashLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.BasketController.cashButtonLabel
        return view
    }()
    
    let cashButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 20).isActive = true
        view.heightAnchor.constraint(equalToConstant: 20).isActive = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    
    let quantityLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.quantityLabel
        return view
    }()
    
    let quantity: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let discountCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.discountCostLabel
        return view
    }()
    
    let discountCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let taxCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.taxCostLabel
        return view
    }()
    
    let taxCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let deliveryCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.deliveryCostLabel
        return view
    }()
    
    let deliveryCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let totalCostLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.OrderController.OrderCell.totalCostLabel
        return view
    }()
    
    let totalCost: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let chooseDateButton: UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedText = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.BasketController.chooseDateButtonLabel, size: 15, color: .black, allignment: .center, underLine: false)
        return view
    }()
    
    let orderButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.BasketController.orderNowButtonLabel, size: 15, color: .black, allignment: .center, underLine: false), for: .normal)
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.black.cgColor
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension BasketCell {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setProviderNameLabelViewConstraints(label: providerNameLabel)
        setProviderNameViewConstraints(label: providerName)
        setProviderAddressLabelViewConstraints(label: providerAddressLabel)
        setProviderAddressViewConstraints(label: providerAddress)
        setProviderPhoneLabelViewConstraints(label: providerPhoneLabel)
        setProviderPhoneViewConstraints(label: providerPhone)
        setDeliveryStatusLabelLabelViewConstraints(label: deliveryStatusLabel)
        setDeliveryStatusStackViewConstraints(label: deliveryStatusStack)
        setPaymentMethodLabelViewConstraints(label: paymentMethodLabel)
        setPaymentMethodStackViewConstraints(label: paymentMethodStack)
        setQuantityLabelViewConstraints(label: quantityLabel)
        setQuantityViewConstraints(label: quantity)
        setDiscountLabelViewConstraints(label: discountCostLabel)
        setDiscountViewConstraints(label: discountCost)
        setTaxLabelViewConstraints(label: taxCostLabel)
        setTaxViewConstraints(label: taxCost)
        setDeliveryCostLabelViewConstraints(label: deliveryCostLabel)
        setDeliveryCostViewConstraints(label: deliveryCost)
        setTotalLabelViewConstraints(label: totalCostLabel)
        setTotalCostViewConstraints(label: totalCost)
        setChooseDateButtonViewConstraints(button: chooseDateButton)
        setOrderButtonViewConstraints(button: orderButton)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(providerNameLabel)
        addSubview(providerName)
        addSubview(providerAddressLabel)
        addSubview(providerAddress)
        addSubview(providerPhoneLabel)
        addSubview(providerPhone)
        addSubview(deliveryStatusLabel)
        addSubview(deliveryStatusStack)
        addSubview(paymentMethodLabel)
        addSubview(paymentMethodStack)
        addSubview(quantityLabel)
        addSubview(quantity)
        addSubview(discountCostLabel)
        addSubview(discountCost)
        addSubview(taxCostLabel)
        addSubview(taxCost)
        addSubview(deliveryCostLabel)
        addSubview(deliveryCost)
        addSubview(totalCostLabel)
        addSubview(totalCost)
        addSubview(chooseDateButton)
        addSubview(orderButton)
//        addSubview(expiryDatePickerView)
    }
    
    private func setProviderNameLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderNameViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: providerNameLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderAddressLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerNameLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderAddressViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerName.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: providerAddressLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderPhoneLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerAddressLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setProviderPhoneViewConstraints(label: UIButton){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerAddress.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: providerPhoneLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDeliveryStatusLabelLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: providerPhoneLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.35),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setDeliveryStatusStackViewConstraints(label: UIStackView){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryStatusLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setPaymentMethodLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryStatusStack.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.35),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setPaymentMethodStackViewConstraints(label: UIStackView){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: paymentMethodLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5),
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    

    private func setQuantityLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setQuantityViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: quantityLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setDiscountLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: quantityLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setDiscountViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: quantity.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: discountCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setTaxLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: discountCostLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setTaxViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: discountCost.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: taxCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setDeliveryCostLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: taxCostLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setDeliveryCostViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: taxCost.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: deliveryCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setTotalLabelViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryCostLabel.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: self.centerXAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setTotalCostViewConstraints(label: UILabel){
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: deliveryCost.bottomAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25),
            label.trailingAnchor.constraint(equalTo: totalCostLabel.leadingAnchor),
            label.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    private func setChooseDateButtonViewConstraints(button: UITextField){
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: totalCost.bottomAnchor, constant: 5),
            button.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            button.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -30),
            button.heightAnchor.constraint(equalToConstant: 30)
            ])
    }
    
    private func setOrderButtonViewConstraints(button: UIButton){
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: chooseDateButton.bottomAnchor, constant: 5),
            button.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            button.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -30),
            button.heightAnchor.constraint(equalToConstant: 30)
            ])
    }
    
}


