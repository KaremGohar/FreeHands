//
//  CategoryCell.swift
//  FreeHands
//
//  Created by Apple on 7/12/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let categoryName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CategoryCell  {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setCategoryLabelViewConstraints(control: categoryName)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(categoryName)
    }
    
    private func setCategoryLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            control.topAnchor.constraint(equalTo: self.topAnchor),
            control.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
    }
    
}
