//
//  LocationCell.swift
//  FreeHands
//
//  Created by Apple on 7/6/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class LocationCell: UICollectionViewCell {
    
    var currentObject: FavourateLocation? {
        didSet {
            if let location = currentObject {
                cellButton.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: location.name!, size: 20, color: .black, allignment: .center, underLine: false), for: .normal)
            }
            cellButton.setTitle(currentObject?.name, for: .normal)
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let cellButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let thumbImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension LocationCell {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setOfferButtonViewConstraints(button: cellButton)
        setOfferImageViewConstraints(image: thumbImage)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(cellButton)
        addSubview(thumbImage)
    }
    
    private func setOfferButtonViewConstraints(button: UIButton){
        NSLayoutConstraint.activate([
            button.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            button.topAnchor.constraint(equalTo: self.topAnchor),
            button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
    }
    
    private func setOfferImageViewConstraints(image: UIImageView){
        NSLayoutConstraint.activate([
            image.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight/2),
            image.widthAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight/2),
            image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: SharedValues.defaultControlHeight/2),
            image.centerYAnchor.constraint(equalTo: self.centerYAnchor)
            ])
    }
    
}
