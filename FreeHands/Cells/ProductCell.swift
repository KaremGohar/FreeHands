//
//  ProductCell.swift
//  FreeHands
//
//  Created by Apple on 7/12/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
//    var produc
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let productPrice: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    let descreption: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    let productName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    let productImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 42
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ProductCell  {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setProviderImageViewConstraints(control: productImage)
        setProviderNameLabelViewConstraints(control: productName)
        setCategoriesLabelViewConstraints(control: descreption)
        setPriceLabelViewConstraints(control: productPrice)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(productImage)
        addSubview(productName)
        addSubview(descreption)
        addSubview(productPrice)
    }
    
    private func setPriceLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            control.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: SharedValues.defaultPadding * 2),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setCategoriesLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: productName.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: productImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setProviderNameLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: self.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: productImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.30)
            ])
    }
    
    private func setProviderImageViewConstraints(control: UIImageView){
        NSLayoutConstraint.activate([
            control.widthAnchor.constraint(equalTo: self.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -2)
            ])
    }
    
}
