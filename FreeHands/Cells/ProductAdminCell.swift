//
//  ProductAdminCell.swift
//  FreeHands
//
//  Created by Apple on 7/17/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class ProductAdminCell: UICollectionViewCell {
    
    var product: Product? {
        didSet{
            productName.text = product?.name
            category.text = product?.category
            descreption.text = product?.description
            productPrice.text = "\(product?.price)"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let productPrice: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    let descreption: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }()
    
    let category: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
                view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 20)
        
        return view
    }()
    
    let productName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textAlignment = .center
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        
        return view
    }()
    
    let productImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 42
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "")
        view.backgroundColor = .cyan
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ProductAdminCell {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = .white
        addSubViewsInsideMainViews()
        setProviderImageViewConstraints(control: productImage)
        setProviderNameLabelViewConstraints(control: productName)
        setCategoriesLabelViewConstraints(control: category)
        setDescriptionLabelViewConstraints(control: descreption)
        setPriceLabelViewConstraints(control: productPrice)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(productImage)
        addSubview(productName)
        addSubview(descreption)
        addSubview(productPrice)
        addSubview(category)
    }
    
    private func setPriceLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            control.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: SharedValues.defaultPadding),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setCategoriesLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: productName.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: productImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setDescriptionLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: category.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: productImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15)
            ])
    }
    
    private func setProviderNameLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: self.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: productImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setProviderImageViewConstraints(control: UIImageView){
        NSLayoutConstraint.activate([
            control.widthAnchor.constraint(equalTo: self.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -1)
            ])
    }
    
}
