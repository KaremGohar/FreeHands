//
//  ProviderCell.swift
//  FreeHands
//
//  Created by Apple on 7/2/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import DJWStarRatingView

class ProviderCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewsConstraints()
    }
    
    let rating : DJWStarRatingView = {
        let view = DJWStarRatingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.starSize = CGSize(width: 20, height: 20)
        view.numberOfStars = 5
        view.fillColor = .yellow
        view.strokeColor = .yellow
        view.unfilledColor = .clear
        view.rating = 2.5
        return view
    }()
    
    let cashImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "dollar")
        return view
    }()
    
    let cardImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "visa")
        return view
    }()
    
    let categories: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    let providerName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    let providerImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 42
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ProviderCell  {
    
    fileprivate func setViewsConstraints() {
        self.backgroundColor = SharedValues.buttonShadowWhiteBackground
        addSubViewsInsideMainViews()
        setProviderImageViewConstraints(control: providerImage)
        setProviderNameLabelViewConstraints(control: providerName)
        setCategoriesLabelViewConstraints(control: categories)
        setCashViewConstraints(control: cashImage)
        setCardViewConstraints(control: cardImage)
        setRatingViewConstraints(control: rating)
    }
    
    private func addSubViewsInsideMainViews(){
        addSubview(providerImage)
        addSubview(providerName)
        addSubview(categories)
        addSubview(cashImage)
        addSubview(cardImage)
        addSubview(rating)
    }
    
    private func setRatingViewConstraints(control: DJWStarRatingView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: categories.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: cardImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: 20),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.3)
            ])
    }
    
    private func setCardViewConstraints(control: UIImageView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: categories.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: cashImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: 15),
            control.widthAnchor.constraint(equalToConstant: 15)
            ])
    }
    
    private func setCashViewConstraints(control: UIImageView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: categories.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: providerImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: 15),
            control.widthAnchor.constraint(equalToConstant: 15)
            ])
    }
    
    private func setCategoriesLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: providerName.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: providerImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setProviderNameLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: self.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: providerImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.30)
            ])
    }
    
    private func setProviderImageViewConstraints(control: UIImageView){
        NSLayoutConstraint.activate([
            control.widthAnchor.constraint(equalTo: self.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.heightAnchor.constraint(equalTo: self.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            control.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: SharedValues.defaultPadding * -1)
            ])
    }
    
}
