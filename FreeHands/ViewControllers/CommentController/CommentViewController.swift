//
//  CommentViewController.swift
//  FreeHands
//
//  Created by Apple on 7/13/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import DJWStarRatingView

class CommentController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(patternImage: SharedValues.ProviderController.backgroundImage)
//        view.backgroundColor = .white
        setViewsConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationControllerItems()
    }
    
    let rateComment: UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = .white
        
        return view
    }()
    
    let rateButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = .brown
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.CommentController.rateNowPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: .center, underLine: false), for: .normal)
        return view
    }()
    
    let userRating : DJWStarRatingView = {
        let view = DJWStarRatingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.starSize = CGSize(width: 25, height: 25)
        view.numberOfStars = 5
        view.fillColor = .yellow 
        view.strokeColor = .yellow
        view.unfilledColor = .clear
        view.rating = 2.5
        return view
    }()
    
    let footerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }()
    
    lazy var commentsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.dataSource = self
        view.delegate = self
        view.register(CommentCell.self, forCellWithReuseIdentifier: SharedValues.CommentController.commentCellId)
        view.backgroundColor = .clear
        return view
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }()
    
    let headerViewTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = .cyan
        view.attributedText = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.CommentController.commentPromptText, size: 25, color: SharedValues.appPreferredTextColor, allignment: .center, underLine: false)
        return view
    }()
    
    let rating : DJWStarRatingView = {
        let view = DJWStarRatingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.starSize = CGSize(width: 30, height: 30)
        view.numberOfStars = 5
        view.fillColor = .yellow
        view.strokeColor = .yellow
        view.unfilledColor = .clear
        view.rating = 2.5
        return view
    }()
    
    let commentTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = .cyan
        view.attributedText = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.CommentController.rateCountPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: .left, underLine: false)
        return view
    }()
    
    let commentsTotal: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.textColor = SharedValues.appPreferredTextColor
        view.textAlignment = .right
        view.font = UIFont.systemFont(ofSize: 15)
        
        return view
    }()
    
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension CommentController {
    
    fileprivate func setupNavigationControllerItems(){
//        navigationItem.title = SharedObject.choosenDeliveryLocation?.addressName
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .black
    }
    
}

extension CommentController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.CommentController.commentCellId, for: indexPath) as! CommentCell
        SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: commentsCollectionView.frame.width, height: SharedValues.defaultControlHeight * 3)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension CommentController {
    
    fileprivate func setViewsConstraints() {
        addSubViewsInsideMainViews()
        setHeaderViewConstraints(control: headerView)
        setHeaderViewTitleViewConstraints(control: headerViewTitle)
        setRatingViewConstraints(control: rating)
        setcommentsTitleLabelViewConstraints(control: commentTitle)
        setcommentsTotalLabelViewConstraints(control: commentsTotal)
        setCommentsCollectionViewConstraints(control: commentsCollectionView)
        setFooterViewConstraints(control: footerView)
        setUserRateViewConstraints(control: userRating)
        setRatingCommentViewTitleViewConstraints(control: rateComment)
        setRateButtonViewTitleViewConstraints(control: rateButton)
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(headerView)
        headerView.addSubview(headerViewTitle)
        headerView.addSubview(rating)
        headerView.addSubview(commentTitle)
        headerView.addSubview(commentsTotal)
        view.addSubview(commentsCollectionView)
        view.addSubview(footerView)
        footerView.addSubview(rateButton)
        footerView.addSubview(rateComment)
        footerView.addSubview(userRating)
    }
    
    private func setRateButtonViewTitleViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: rateComment.bottomAnchor, constant: SharedValues.defaultPadding),
            control.centerXAnchor.constraint(equalTo: footerView.centerXAnchor),
            control.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 0.15),
            control.widthAnchor.constraint(equalTo: footerView.widthAnchor, multiplier: 0.30)
            ])
    }
    
    private func setRatingCommentViewTitleViewConstraints(control: UITextView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: userRating.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 0.4),
            control.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setUserRateViewConstraints(control: DJWStarRatingView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: footerView.topAnchor, constant: SharedValues.defaultPadding),
            control.widthAnchor.constraint(equalTo: footerView.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 0.15),
            control.centerXAnchor.constraint(equalTo: footerView.centerXAnchor)
            ])
    }
    
    private func setFooterViewConstraints(control: UIView){
        NSLayoutConstraint.activate([
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: SharedMethod.calculateTabBarHeight(tabController: self.tabBarController) * -1),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 4),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    private func setCommentsCollectionViewConstraints(control: UICollectionView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.bottomAnchor.constraint(equalTo: footerView.topAnchor, constant: SharedValues.defaultPadding * -1),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setHeaderViewConstraints(control: UIView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: view.topAnchor, constant: SharedMethod.calculateNavigationBarHeight(navigationController: self.navigationController) + SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 4),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    private func setHeaderViewTitleViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerView.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.2),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.35)
            ])
    }
    
    private func setRatingViewConstraints(control: DJWStarRatingView){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
            control.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.2),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.5)
            ])
    }
    
    private func setcommentsTitleLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.leadingAnchor.constraint(equalTo: headerView.centerXAnchor, constant: SharedValues.defaultPadding),
            control.topAnchor.constraint(equalTo: rating.bottomAnchor, constant: SharedValues.defaultPadding),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.15),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.2)
            ])
    }
    
    private func setcommentsTotalLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.trailingAnchor.constraint(equalTo: headerView.centerXAnchor, constant: SharedValues.defaultPadding * -1),
            control.topAnchor.constraint(equalTo: rating.bottomAnchor, constant: SharedValues.defaultPadding),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.15),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.2)
            ])
    }
    
}
