//
//  OfferCollectionViewController.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class OfferCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(OfferCell.self, forCellWithReuseIdentifier: SharedValues.OfferController.offerCellId)
        collectionView?.backgroundView = backgroundImageView
        setNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.OfferController.offerCellId, for: indexPath) as! OfferCell
    
        cell.offerImage.image = UIImage(named: "offerBackground")
        cell.offerLabel.text = "Label"
        cell.offerProviderName.text = "Provider"
        cell.offerPrice.text = "price"
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width , height: 150)
    }
    
    let backgroundImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.backgroundImage
        view.contentMode = .scaleAspectFit
        return view
    }()

    fileprivate func setNavigationBar(){
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.title = SharedValues.OfferController.navigationBarTitle
    }
}
