//
//  ProviderCollectionViewController.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class ProviderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewConstraits()
        setNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setPageControls()
        if SharedObject.providerViewController.viewControllers.count > 1 {
            SharedObject.providerViewController.viewControllers.remove(at: 2)
        }
    }
    
    private func setPageControls(){
        if let addressName = SharedObject.choosenDeliveryLocation?.addressName {
            locationButton.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: addressName, size: 15, color: SharedValues.ProviderController.locationButtonTitleColor, allignment: .center, underLine: false), for: .normal)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    let backgroundImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.backgroundImage
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let logoImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.logoImage
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var locationButton : UIButton = {
        let view = UIButton(type: .system)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.ProviderController.locationButtonTitle, size: 15, color: SharedValues.ProviderController.locationButtonTitleColor, allignment: .center, underLine: false), for: .normal)
        view.addTarget(self, action: #selector(handleLocationAction), for: .touchUpInside)
        return view
    }()
    
    lazy var searchButton : UIButton = {
        let view = UIButton(type: .system)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = SharedValues.ProviderController.searchBackgroundColor
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.ProviderController.searchButtonTitle, size: 20, color: SharedValues.ProviderController.searchButtonTitleColor, allignment: .center, underLine: false), for: .normal)
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.addTarget(self, action: #selector(handleSearchAction), for: .touchUpInside)
        return view
    }()

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension ProviderViewController {
    
    @objc fileprivate func handleSearchAction(){
        if let address = SharedObject.choosenDeliveryLocation {
            let controller = ProviderSearchResultController()
            navigationController?.pushViewController(controller, animated: true)
        } else {
            handleLocationAction()
        }
    }
    
    @objc fileprivate func handleLocationAction(){
        navigationController?.pushViewController(SharedObject.favourateLocationsController, animated: true)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension ProviderViewController {
    
    func setViewConstraits(){
        addSubViewsInsideMainViews()
        setBackgroundImageView(image: backgroundImageView)
        setLogoImageView(image: logoImageView)
        setLocationButonConstraints(button: locationButton)
        setSearchButonConstraints(button: searchButton)
    }
    
    func addSubViewsInsideMainViews(){
        view.addSubview(backgroundImageView)
        view.addSubview(logoImageView)
        view.addSubview(locationButton)
        view.addSubview(searchButton)
    }
    
    fileprivate func setNavigationBar(){
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.title = SharedValues.ProviderController.navigationBarTitle
    }

    private func setLocationButonConstraints(button: UIButton){
        NSLayoutConstraint.activate([
            button.bottomAnchor.constraint(equalTo: searchButton.topAnchor, constant: -20),
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            button.heightAnchor.constraint(equalToConstant: 30)
            ])
    }
    
    private func setSearchButonConstraints(button: UIButton){
        NSLayoutConstraint.activate([
            button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5),
            button.heightAnchor.constraint(equalToConstant: 40)
            ])
    }
    
    private func setLogoImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor, constant: 150),
            image.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            image.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6),
            image.heightAnchor.constraint(equalToConstant: 70)
            ])
    }
    
    private func setBackgroundImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor),
            image.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
