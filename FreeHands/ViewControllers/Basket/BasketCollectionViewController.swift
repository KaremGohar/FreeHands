//
//  BasketCollectionViewController.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class BasketViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var choosenDate: String?
    private var choosenDateIndex: Int?
    var basketItems: [BasketItem] = [BasketItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundColor = .white
//        view.backgroundView = backgroundImageView
        setNavigationBar()
        setViewsConstraints()
    }
    
    lazy var basketCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.dataSource = self
        view.delegate = self
        view.register(BasketCell.self, forCellWithReuseIdentifier: SharedValues.BasketController.basketCellId)
        view.backgroundView = backgroundImageView
        return view
    }()

    private func createInputViewToolBar() -> UIToolbar {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        return toolbar
    }

    @objc private func donePressed() {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        let dateString = formatter.string(from: expiryDatePickerView.date)
//        textField.text = "\(dateString)"
        choosenDate = dateString
//        basketItems[choosenDateIndex!].date = dateString
        print(choosenDateIndex)
        print(choosenDate)
        self.view.endEditing(true)
    }
    
    fileprivate lazy var expiryDatePickerView: UIDatePicker = UIDatePicker()

    let backgroundImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.backgroundImage
        view.contentMode = .scaleAspectFill
        return view
    }()

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension //////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension BasketViewController {
    
    fileprivate func setNavigationBar(){
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.title = SharedValues.BasketController.navigationBarTitle
    }
    
}

extension BasketViewController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        choosenDateIndex = indexPath.item
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.BasketController.basketCellId, for: indexPath) as! BasketCell
        SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
        cell.chooseDateButton.inputView = expiryDatePickerView
        cell.chooseDateButton.inputAccessoryView = createInputViewToolBar()
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width , height: 200)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension BasketViewController {
    
    fileprivate func setViewsConstraints() {
        addSubViewsInsideMainViews()
        setBasketCollectionViewConstraints(control: basketCollectionView)
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(basketCollectionView)
    }
    
    private func setBasketCollectionViewConstraints(control: UICollectionView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: view.topAnchor, constant: SharedMethod.calculateNavigationBarHeight(navigationController: navigationController)),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: SharedMethod.calculateTabBarHeight(tabController: tabBarController) * -1),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
}



