//
//  ProviderProductController.swift
//  FreeHands
//
//  Created by Apple on 7/12/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import DJWStarRatingView

class ProviderProductController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var currentProvider: Provider?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundView = UIImageView(image: SharedValues.ProviderController.backgroundImage)
        view.backgroundColor = UIColor(patternImage: SharedValues.ProviderController.backgroundImage)
        setViewsConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationControllerItems()
    }
    
    let providerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var categoriesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.register(CategoryCell.self, forCellWithReuseIdentifier: SharedValues.ProviderProductController.categoryCellId)
        layout.scrollDirection = .horizontal
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var productsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.register(ProductCell.self, forCellWithReuseIdentifier: SharedValues.ProviderProductController.productCellId)
        view.backgroundColor = .clear
        return view
    }()
    
    let rating : DJWStarRatingView = {
        let view = DJWStarRatingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.starSize = CGSize(width: 20, height: 20)
        view.numberOfStars = 5
        view.fillColor = .yellow
        view.strokeColor = .yellow
        view.unfilledColor = .clear
        view.rating = 2.5
        return view
    }()
    
    let comments: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = .blue
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.ProviderProductController.commentsPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: .center, underLine: false), for: .normal)
        view.addTarget(self, action: #selector(gotoComments), for: .touchUpInside)
        return view
    }()
    
    let categories: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .blue
        
        return view
    }()
    
    let providerName: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .blue
        
        return view
    }()
    
    let providerImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 42
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFit
        view.backgroundColor = .cyan
        return view
    }()
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension ProviderProductController {
    
    @objc fileprivate func gotoComments(){
        navigationController?.pushViewController(CommentController(), animated: true)
    }
    
    private func setupRightNavItems(){
        let searchButton = UIButton(type: .system)
        searchButton.setImage(#imageLiteral(resourceName: "search"), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        let actionSheetButton = UIButton(type: .system)
        actionSheetButton.setImage(#imageLiteral(resourceName: "circles"), for: .normal)
        actionSheetButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: actionSheetButton)]
    }
    
    fileprivate func setupNavigationControllerItems(){
//        navigationItem.title = SharedObject.choosenDeliveryLocation?.addressName
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .black
    }
    
}

extension ProviderProductController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == productsCollectionView {
            return 5
        } else {
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == productsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.ProviderProductController.productCellId, for: indexPath) as! ProductCell
            cell.productImage.image = UIImage(named: "pic6")
            SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.ProviderProductController.categoryCellId, for: indexPath) as! CategoryCell
            SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productsCollectionView {
            return CGSize(width: view.frame.width, height: SharedValues.defaultControlHeight*3)
        } else {
            return CGSize(width: view.frame.width / 4, height: SharedValues.defaultControlHeight)
        }
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension ProviderProductController {
    
    fileprivate func setViewsConstraints() {
        addSubViewsInsideMainViews()
        setProviderViewConstraints(control: providerView)
        setCategoryCollectionViewConstraints(control: categoriesCollectionView)
        setProductCollectionViewConstraints(control: productsCollectionView)
        setProviderImageViewConstraints(control: providerImage)
        setProviderNameLabelViewConstraints(control: providerName)
        setCategoriesLabelViewConstraints(control: categories)
        setRatingViewConstraints(control: rating)
        setCommentsButtonViewConstraints(control: comments)
        setupRightNavItems()
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(categoriesCollectionView)
        view.addSubview(productsCollectionView)
        view.addSubview(providerView)
        providerView.addSubview(providerImage)
        providerView.addSubview(providerName)
        providerView.addSubview(categories)
        providerView.addSubview(rating)
        providerView.addSubview(comments)
    }
    
    private func setProductCollectionViewConstraints(control: UICollectionView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: categoriesCollectionView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: SharedMethod.calculateTabBarHeight(tabController: self.tabBarController) * -1),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setCategoryCollectionViewConstraints(control: UICollectionView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: providerView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    private func setProviderViewConstraints(control: UIView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: view.topAnchor, constant: SharedMethod.calculateNavigationBarHeight(navigationController: self.navigationController) + SharedValues.defaultPadding * 3),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 4),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    private func setCommentsButtonViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: categories.bottomAnchor, constant: SharedValues.defaultPadding),
            control.leadingAnchor.constraint(equalTo: providerView.leadingAnchor, constant: SharedValues.defaultPadding),
            control.widthAnchor.constraint(equalTo: providerView.widthAnchor, multiplier: 0.2),
            control.heightAnchor.constraint(equalTo: providerView.heightAnchor, multiplier: 0.15)
            ])
    }
    
    private func setRatingViewConstraints(control: DJWStarRatingView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: categories.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: providerImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: 20),
            control.widthAnchor.constraint(equalTo: providerView.widthAnchor, multiplier: 0.3)
            ])
    }
    
    private func setCategoriesLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: providerName.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: providerImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: providerView.widthAnchor, multiplier: 0.6),
            control.heightAnchor.constraint(equalTo: providerView.heightAnchor, multiplier: 0.20)
            ])
    }
    
    private func setProviderNameLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: providerView.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: providerImage.leadingAnchor, constant: SharedValues.defaultPadding * -1),
            control.widthAnchor.constraint(equalTo: providerView.widthAnchor, multiplier: 0.4),
            control.heightAnchor.constraint(equalTo: providerView.heightAnchor, multiplier: 0.30)
            ])
    }
    
    private func setProviderImageViewConstraints(control: UIImageView){
        NSLayoutConstraint.activate([
            control.widthAnchor.constraint(equalTo: providerView.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.heightAnchor.constraint(equalTo: providerView.heightAnchor, constant: SharedValues.defaultPadding * -2),
            control.centerYAnchor.constraint(equalTo: providerView.centerYAnchor),
            control.trailingAnchor.constraint(equalTo: providerView.trailingAnchor, constant: SharedValues.defaultPadding * -1)
            ])
    }
    
}
