//
//  ProviderAdminController.swift
//  FreeHands
//
//  Created by Apple on 7/17/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import DJWStarRatingView

class ProviderAdminController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(patternImage: SharedValues.ProviderController.backgroundImage)
        setViewConstraits()
    }
    
    
    lazy var productCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(ProductAdminCell.self, forCellWithReuseIdentifier: SharedValues.ProviderAdminController.providerAdminCellId)
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = .clear
        return view
    }()
    
    let addProductButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setBackgroundImage(UIImage(named: "plus_1"), for: .normal)
        view.addTarget(self, action: #selector(handleAddProduct), for: .touchUpInside)
        return view
    }()
    
    let productTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .right
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 20)
        view.text = "Products"
        return view
    }()
    
    let productView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 235, green: 177, blue: 68)
        
        return view
    }()
    
    let availaibleTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        //        view.backgroundColor = .cyan
        view.textAlignment = .center
        view.textColor = .white
        view.font = UIFont.systemFont(ofSize: 15)
        view.text = "Available"
        view.backgroundColor = UIColor(red: 235, green: 177, blue: 68)
        return view
    }()
    
    let availableSwitch: UISwitch = {
        let view = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(toggleState), for: .valueChanged)
        
        return view
    }()
    
    let rating : DJWStarRatingView = {
        let view = DJWStarRatingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.starSize = CGSize(width: 20, height: 20)
        view.numberOfStars = 5
        view.fillColor = .black
        view.strokeColor = .black
        view.unfilledColor = .clear
        view.rating = 3.4
        return view
    }()
    
    let locationTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        //        view.backgroundColor = .cyan
        view.textAlignment = .right
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 15)
        view.text = "Cairo, Egypt"
        return view
    }()
    
    let headerViewTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        //        view.backgroundColor = .cyan
        view.textAlignment = .right
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        view.text = "Ahmed Haweel"
        return view
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.backgroundColor = .red
        
        return view
    }()
    
    let personImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "person")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
}

extension ProviderAdminController {
    
    @objc fileprivate func handleAddProduct(){
        print("Add Product")
    }
    
    @objc fileprivate func toggleState(){
        if availableSwitch.isOn {
            print("On")
        } else {
            print("Off")
        }
    }
    
}

extension ProviderAdminController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.ProviderAdminController.providerAdminCellId, for: indexPath) as! ProductAdminCell
        SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: productCollectionView.frame.width, height: SharedValues.defaultControlHeight * 3)
    }
    
}

extension ProviderAdminController {
    
    func setViewConstraits(){
        addSubViewsInsideMainViews()
        setHeaderViewConstraints(control: headerView)
        setTemplateImageView(image: personImageView)
        setHeaderViewTitleViewConstraints(control: headerViewTitle)
        setLocationTitleViewConstraints(control: locationTitle)
        setRatingViewConstraints(control: rating)
        setAvailableSwitchViewConstraints(control: availableSwitch)
        setAvailableLabelViewConstraints(control: availaibleTitle)
        setProductViewConstraints(control: productView)
        setProductLabelViewConstraints(control: productTitle)
        setAddProductButtonViewConstraints(control: addProductButton)
        setProductCollectionViewConstraints(control: productCollectionView)
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(personImageView)
        view.addSubview(headerView)
        view.addSubview(headerViewTitle)
        view.addSubview(locationTitle)
        view.addSubview(rating)
        view.addSubview(availableSwitch)
        view.addSubview(availaibleTitle)
        view.addSubview(productView)
        view.addSubview(productTitle)
        view.addSubview(addProductButton)
        view.addSubview(productCollectionView)
    }
    
    private func setProductCollectionViewConstraints(control: UICollectionView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: productView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: SharedMethod.calculateTabBarHeight(tabController: tabBarController) * -1),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setAddProductButtonViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.centerYAnchor.constraint(equalTo: productView.centerYAnchor),
            control.leadingAnchor.constraint(equalTo: productView.leadingAnchor, constant: SharedValues.defaultPadding),
            control.heightAnchor.constraint(equalTo: productView.heightAnchor, multiplier: 0.8),
            control.widthAnchor.constraint(equalTo: productView.widthAnchor, multiplier: 0.1)
            ])
    }
    
    private func setProductLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.centerYAnchor.constraint(equalTo: productView.centerYAnchor),
            control.trailingAnchor.constraint(equalTo: productView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: productView.heightAnchor),
            control.widthAnchor.constraint(equalTo: productView.widthAnchor, multiplier: 0.4)
            ])
    }
    
    private func setProductViewConstraints(control: UIView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 1),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setAvailableLabelViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerView.topAnchor, constant: SharedValues.defaultPadding),
            control.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: SharedValues.defaultPadding),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.2),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.2)
            ])
    }
    
    private func setAvailableSwitchViewConstraints(control: UISwitch){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: availaibleTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: SharedValues.defaultPadding),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.2),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.2)
            ])
    }
    
    private func setRatingViewConstraints(control: DJWStarRatingView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: locationTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.15),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.3)
            ])
    }
    
    private func setLocationTitleViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerViewTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.15),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.4)
            ])
    }
    
    private func setHeaderViewTitleViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerView.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.2),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.6)
            ])
    }
    
    private func setHeaderViewConstraints(control: UIView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: personImageView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 4),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setTemplateImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor, constant: SharedMethod.calculateNavigationBarHeight(navigationController: navigationController)),
            image.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 5),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
}
