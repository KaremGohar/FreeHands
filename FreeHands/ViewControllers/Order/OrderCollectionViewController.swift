//
//  OrderCollectionViewController.swift
//  FreeHands
//
//  Created by Apple on 7/1/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class OrderCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(OrderCell.self, forCellWithReuseIdentifier: SharedValues.OrderController.orderCellId)
        collectionView?.backgroundView = backgroundImageView
        setNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.OrderController.orderCellId, for: indexPath) as! OrderCell
    
        cell.providerName.text = "Testing"
        cell.orderStatusProgressBar.progress = 0.25
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width , height: 140)
    }
    
    fileprivate func setNavigationBar(){
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.title = SharedValues.OrderController.navigationBarTitle
    }
    
    let backgroundImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.backgroundImage
        view.contentMode = .scaleAspectFit
        return view
    }()

}
