//
//  LoginController.swift
//  FreeHands
//
//  Created by Apple on 7/4/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class LoginController: UIViewController {
    
    private var userType: String = SharedValues.LoginController.providerType

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewConstraits()
    }
    
    let rememberMeLabel : UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = SharedValues.LoginController.rememberMePromptText
        return view
    }()
    
    let rememberMeButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 20).isActive = true
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var rememberMeStack : UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .equalCentering
        view.addArrangedSubview(rememberMeLabel)
        view.addArrangedSubview(rememberMeButton)
        return view
    }()
    
    let fingerPrintActionButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.LoginController.loginWithFingerPrintPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false), for: .normal)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    let handlePageActionButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.LoginController.loginPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false), for: .normal)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        view.addTarget(self, action: #selector(handleAction), for: .touchUpInside)
        return view
    }()
    
    let magicTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.magicPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var magicImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "passwordIcon-Small")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let nicknameTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.nicknamePromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var driverProviderSegmentedControl : UISegmentedControl = {
        let view = UISegmentedControl(items: [SharedValues.RegisterController.providerSegmentTitle, SharedValues.RegisterController.driverSegmentTitle])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.selectedSegmentIndex = 0
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.tintColor = SharedValues.appPreferredTextColor
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        view.addTarget(self, action: #selector(toggleSegment), for: .valueChanged)
        return view
    }()
    
    var scrollView: UIScrollView = {
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        var view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view = UIScrollView(frame: CGRect(x: 0, y: 200+SharedValues.distanceBetweenControls, width: screenWidth, height: screenHeight))
        view.contentSize = CGSize(width: screenWidth, height: screensize.height)
        return view
    }()
    
    let logoImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.logoImage
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    let backgroundImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.backgroundImage
        view.contentMode = .scaleAspectFill
        return view
    }()

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension //////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension LoginController {
    
    @objc func toggleSegment(){
        if driverProviderSegmentedControl.selectedSegmentIndex == 0 {
            userType = SharedValues.LoginController.providerType
        } else {
            userType = SharedValues.LoginController.driverType
        }
    }
    
    @objc fileprivate func handleAction(){
        if userType == SharedValues.LoginController.providerType {
            driverLogin()
        } else {
            providerLogin()
        }
    }
    
    private func driverLogin(){
        
    }
    
    private func providerLogin(){
        
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension LoginController {
    
    func setViewConstraits(){
        addSubViewsInsideMainViews()
        setBackgroundImageView(image: backgroundImageView)
        setLogoImageView(image: logoImageView)
        setDriverProviderSegmentConstraints(control: driverProviderSegmentedControl)
        setnicknameTextViewContraint(addedView: nicknameTextField)
        setMagicTextViewContraint(addedView: magicTextField)
        setMagicImageView(image: magicImageView)
        setRegisterButtonViewConstraints(control: handlePageActionButton)
        setFingerPrintButtonViewConstraints(control: fingerPrintActionButton)
        setRememberMeStackViewConstraints(control: rememberMeStack)
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(backgroundImageView)
        view.addSubview(logoImageView)
        view.addSubview(driverProviderSegmentedControl)
        view.addSubview(nicknameTextField)
        view.addSubview(magicTextField)
        view.addSubview(magicImageView)
        view.addSubview(handlePageActionButton)
        view.addSubview(fingerPrintActionButton)
        view.addSubview(rememberMeStack)
    }
    
    private func setRememberMeStackViewConstraints(control: UIStackView){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.topAnchor.constraint(equalTo: fingerPrintActionButton.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.40),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setRememberMeButtonViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.widthAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setFingerPrintButtonViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.topAnchor.constraint(equalTo: handlePageActionButton.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.60),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setRegisterButtonViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.topAnchor.constraint(equalTo: magicTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.60),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setMagicTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addedView.topAnchor.constraint(equalTo: nicknameTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setMagicImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: magicTextField.leadingAnchor, constant: 10),
            image.centerYAnchor.constraint(equalTo: magicTextField.centerYAnchor),
            image.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setnicknameTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addedView.topAnchor.constraint(equalTo: driverProviderSegmentedControl.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setDriverProviderSegmentConstraints(control: UISegmentedControl){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: SharedValues.distanceBetweenControls*3),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.65),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setScrollView(control: UIScrollView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: view.topAnchor),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
    private func setLogoImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            image.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            image.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6),
            image.heightAnchor.constraint(equalToConstant: 70)
            ])
    }
    
    private func setBackgroundImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor),
            image.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
}
