//
//  RegisterController.swift
//  FreeHands
//
//  Created by Apple on 7/3/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class RegisterController: UIViewController {
    
    var selectedImageView: Bool = true
    private var provider = Provider()

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewConstraits()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    let handlePageActionButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.registerPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false), for: .normal)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        view.addTarget(self, action: #selector(handlePageAction), for: .touchUpInside)
        return view
    }()
    
    let cityTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.cityPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        view.isHidden = true
        return view
    }()
    
    let phoneTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.phonePromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var phoneImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "mobileIcon-Small")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let locationButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.locationPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false), for: .normal)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var locationImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "locationIcon-Small")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let confirmMagicTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.confirmMagicPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var confirmMagicImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "passwordIcon-Small")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let magicTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.magicPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var magicImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "passwordIcon-Small")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let emailTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.emailPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var emailImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "emailIcon-Small")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    let nicknameTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.nicknamePromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    let firstnameTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.firstnamePromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    let lastnameTextField : UITextField = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.attributedPlaceholder = SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.lastnamePromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false)
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.layer.cornerRadius = SharedValues.defaultControlHeight/2
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var profileImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(setProfileImage)))
        view.isUserInteractionEnabled = true
        view.layer.cornerRadius = 50
        view.layer.masksToBounds = true
        view.image = UIImage(named: "user-72")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var idImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(setIdImage)))
        view.isUserInteractionEnabled = true
        view.layer.cornerRadius = 50
        view.layer.masksToBounds = true
        view.image = UIImage(named: "idIcon-76")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var driverProviderSegmentedControl : UISegmentedControl = {
        let view = UISegmentedControl(items: [SharedValues.RegisterController.providerSegmentTitle, SharedValues.RegisterController.driverSegmentTitle])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.selectedSegmentIndex = 0
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.tintColor = SharedValues.appPreferredTextColor
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        view.addTarget(self, action: #selector(toggleSegment), for: .valueChanged)
        return view
    }()
    
    lazy var templateImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "pic2")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    var scrollView: UIScrollView = {
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        var view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view = UIScrollView(frame: CGRect(x: 0, y: 200+SharedValues.distanceBetweenControls, width: screenWidth, height: screenHeight))
        view.contentSize = CGSize(width: screenWidth, height: screensize.height*1.5)
        return view
    }()
    
    
    let backgroundImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderController.backgroundImage
        view.contentMode = .scaleAspectFill
        return view
    }()

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension //////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension RegisterController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @objc fileprivate func handlePageAction(){
        print("Register Pressed")
        provider.Name = "test"
        provider.FirstName = "test"
        provider.LastName = "test"
        provider.TradeName = "test"
        provider.Email = "test"
        provider.Password = "test"
        provider.Lat = "test"
        provider.Log = "test"
        provider.Location = "test"
        provider.Phone = "test"
        provider.KnownNumber = "test"
        provider.Provider_Photo = "test"
        provider.Provider_Identity = "test"
        SharedObject.providerService.Register(parameters: provider, completionHandler: { (data) -> (Void) in
            print(data)
        }) { (error) in
            print(error)
        }
    }
    
    @objc func toggleSegment(){
        if driverProviderSegmentedControl.selectedSegmentIndex == 0 {
            locationButton.isHidden = !locationButton.isHidden
//            handlePageActionButton.isHidden = !handlePageActionButton.isHidden
            nicknameTextField.isHidden = !nicknameTextField.isHidden
            cityTextField.isHidden = !cityTextField.isHidden
            templateImageView.image = UIImage(named: "pic2")
            handlePageActionButton.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.registerPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false), for: .normal)
            UIApplication.shared.statusBarStyle = .default
        } else {
            locationButton.isHidden = !locationButton.isHidden
//            handlePageActionButton.isHidden = !handlePageActionButton.isHidden
            nicknameTextField.isHidden = !nicknameTextField.isHidden
            cityTextField.isHidden = !cityTextField.isHidden
            templateImageView.image = UIImage(named: "person")
            handlePageActionButton.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.RegisterController.savePromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: NSTextAlignment.center, underLine: false), for: .normal)
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
    
    @objc func setIdImage(){
        selectedImageView = false
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func setProfileImage(){
        selectedImageView = true
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage : UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImage = editedImage
            print("Choosed edited image")
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = originalImage
            print("Choosed original image")
        }
        if let image = selectedImage {
            if selectedImageView {
                profileImageView.image = image
            } else {
                idImageView.image = image
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension RegisterController {
    
    func setViewConstraits(){
        addSubViewsInsideMainViews()
        setBackgroundImageView(image: backgroundImageView)
        setProfileImageView(image: profileImageView)
        setIdImageView(image: idImageView)
        setDriverProviderSegmentConstraints(control: driverProviderSegmentedControl)
        setFirstnameTextViewContraint(addedView: firstnameTextField)
        setLastnameTextViewContraint(addedView: lastnameTextField)
        setnicknameTextViewContraint(addedView: nicknameTextField)
        setemailTextViewContraint(addedView: emailTextField)
        setEmailImageView(image: emailImageView)
        setMagicTextViewContraint(addedView: magicTextField)
        setMagicImageView(image: magicImageView)
        setConfirmMagicTextViewContraint(addedView: confirmMagicTextField)
        setConfirmMagicImageView(image: confirmMagicImageView)
        setLocationButtonViewContraint(control: locationButton)
        setCityButtonViewContraint(control: cityTextField)
        setLocationImageView(control: locationImageView)
        setPhoneTextViewContraint(addedView: phoneTextField)
        setPhoneImageView(image: phoneImageView)
        setRegisterButtonViewConstraints(control: handlePageActionButton)
        setTemplateImageView(image: templateImageView)
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(backgroundImageView)
        view.addSubview(templateImageView)
        view.addSubview(scrollView)
        scrollView.addSubview(profileImageView)
        scrollView.addSubview(idImageView)
        scrollView.addSubview(driverProviderSegmentedControl)
        scrollView.addSubview(firstnameTextField)
        scrollView.addSubview(lastnameTextField)
        scrollView.addSubview(nicknameTextField)
        scrollView.addSubview(emailTextField)
        scrollView.addSubview(emailImageView)
        scrollView.addSubview(magicTextField)
        scrollView.addSubview(magicImageView)
        scrollView.addSubview(confirmMagicTextField)
        scrollView.addSubview(confirmMagicImageView)
        scrollView.addSubview(locationButton)
        scrollView.addSubview(cityTextField)
        scrollView.addSubview(locationImageView)
        scrollView.addSubview(phoneTextField)
        scrollView.addSubview(phoneImageView)
        scrollView.addSubview(handlePageActionButton)
    }
    
    private func setRegisterButtonViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: SharedValues.defaultControlHeight*2 + SharedValues.distanceBetweenControls),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.60),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setPhoneTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addedView.topAnchor.constraint(equalTo: locationButton.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setPhoneImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: phoneTextField.leadingAnchor, constant: 10),
            image.centerYAnchor.constraint(equalTo: phoneTextField.centerYAnchor),
            image.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setCityButtonViewContraint(control : UITextField) {
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.topAnchor.constraint(equalTo: confirmMagicTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setLocationButtonViewContraint(control : UIButton) {
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.topAnchor.constraint(equalTo: confirmMagicTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setLocationImageView(control: UIImageView){
        NSLayoutConstraint.activate([
            control.leadingAnchor.constraint(equalTo: locationButton.leadingAnchor, constant: 10),
            control.centerYAnchor.constraint(equalTo: locationButton.centerYAnchor),
            control.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setConfirmMagicTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addedView.topAnchor.constraint(equalTo: magicTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setConfirmMagicImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: confirmMagicTextField.leadingAnchor, constant: 10),
            image.centerYAnchor.constraint(equalTo: confirmMagicTextField.centerYAnchor),
            image.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setMagicTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addedView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setMagicImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: magicTextField.leadingAnchor, constant: 10),
            image.centerYAnchor.constraint(equalTo: magicTextField.centerYAnchor),
            image.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setemailTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addedView.topAnchor.constraint(equalTo: firstnameTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setEmailImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: emailTextField.leadingAnchor, constant: 10),
            image.centerYAnchor.constraint(equalTo: emailTextField.centerYAnchor),
            image.heightAnchor.constraint(equalToConstant: 20)
            ])
    }
    
    private func setnicknameTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            addedView.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setFirstnameTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: 12),
            addedView.topAnchor.constraint(equalTo: driverProviderSegmentedControl.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.40),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setLastnameTextViewContraint(addedView : UITextField) {
        NSLayoutConstraint.activate([
            addedView.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: -12),
            addedView.topAnchor.constraint(equalTo: driverProviderSegmentedControl.bottomAnchor, constant: SharedValues.distanceBetweenControls),
            addedView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.40),
            addedView.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setDriverProviderSegmentConstraints(control: UISegmentedControl){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            control.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 125),
            control.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.65),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setProfileImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: SharedValues.distanceBetweenControls),
            image.topAnchor.constraint(equalTo: scrollView.topAnchor),
            image.widthAnchor.constraint(equalToConstant: 100),
            image.heightAnchor.constraint(equalToConstant: 100)
            ])
    }
    
    @objc private func setIdImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.trailingAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: -SharedValues.distanceBetweenControls),
            image.topAnchor.constraint(equalTo: scrollView.topAnchor),
            image.widthAnchor.constraint(equalToConstant: 100),
            image.heightAnchor.constraint(equalToConstant: 100)
            ])
    }
    
    private func setTemplateImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor),
            image.heightAnchor.constraint(equalToConstant: 200),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
    private func setScrollView(control: UIScrollView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: view.topAnchor),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
    private func setBackgroundImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor),
            image.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
}
