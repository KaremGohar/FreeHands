//
//  LocalListRepository.swift
//  FreeHands
//
//  Created by Apple on 7/9/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import Foundation

class LocalListRepository<Generic> where Generic: NSCoding {
    
    func saveListWith(key: String, list: [Generic]){
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: list)
        UserDefaults.standard.set(encodedData, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func getListFor(key: String) -> [Generic]{
        let decoded  = UserDefaults.standard.object(forKey: key) as! Data
        return NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [Generic]
    }
    
}
