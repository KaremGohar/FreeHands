//
//  MapController.swift
//  FreeHands
//
//  Created by Apple on 7/8/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    private var isSave: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setViewsConstraints()
        setNavigationBar()
        setupLocationManager()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SharedObject.locationManager?.startUpdatingLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SharedObject.locationManager?.stopUpdatingLocation()
        mapKitView.removeAnnotations(mapKitView.annotations)
        cleanUpLocationManager()
    }
    
    let myLocationButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setBackgroundImage(UIImage(named: "currentLocationIcon-Small-50"), for: .normal)
        view.addTarget(self, action: #selector(centerUserLocationOnMapView), for: .touchUpInside)
        return view
    }()
    
    let selectedLocationImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "mapPinIcon-Small")
        return view
    }()
    
    let handleActionButton : UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = SharedValues.buttonShadowWhiteBackground
        view.setAttributedTitle(SharedMethod.buildPlaceHolderTextWith(string: SharedValues.MapController.selectPromptText, size: 15, color: SharedValues.appPreferredTextColor, allignment: .center, underLine: false), for: .normal)
        view.addTarget(self, action: #selector(handleSelectAction), for: .touchUpInside)
        return view
    }()

    let mapKitView : MKMapView = {
        let view = MKMapView()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.mapType = MKMapType.hybrid
        return view
    }()
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension MapController {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        SharedObject.locationManager?.distanceFilter = SharedValues.distanceFilter
        self.mapKitView.showsUserLocation = true
        updateUserCurrentLocation(location: location)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        handleMapScroll(mapView: mapView)
    }
    
    fileprivate func handleMapScroll(mapView: MKMapView){
        var center = mapView.centerCoordinate
    }
    
    fileprivate func updateUserCurrentLocation(location : CLLocation){
        SharedObject.userLocation = location
    }
    
    @objc fileprivate func centerUserLocationOnMapView(){
        if let location = SharedObject.userLocation?.coordinate {
            mapKitView.setCenter(location, animated: true)
        } else {
            SharedObject.locationManager?.requestAlwaysAuthorization()
            SharedObject.locationManager?.requestWhenInUseAuthorization()
        }
    }
    
    fileprivate func setupLocationManager(){
        SharedObject.locationManager = CLLocationManager()
        mapKitView.delegate = self
        SharedObject.locationManager?.delegate = self
        SharedObject.locationManager?.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        SharedObject.locationManager?.requestAlwaysAuthorization()
        SharedObject.locationManager?.requestWhenInUseAuthorization()
    }
    
    fileprivate func cleanUpLocationManager(){
        SharedObject.locationManager = nil
        mapKitView.delegate = nil
        SharedObject.locationManager?.delegate = nil
//        mapKitView.removeFromSuperview()
    }
    
    fileprivate func setNavigationBar(){
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.title = SharedValues.MapController.navigationBarTitle
    }
    
    @objc fileprivate func handleSelectAction(){
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: SharedValues.MapController.savePromptText, style: .default) { [weak self] _ in
            
            let locationAlertController = UIAlertController(title: nil, message: SharedValues.MapController.pleaseEnterLocationName, preferredStyle: .alert)
            locationAlertController.addTextField { (textField : UITextField!)  -> Void in
                textField.placeholder = SharedValues.MapController.locationNamePromptText
            }
            let saveAction = UIAlertAction(title: SharedValues.MapController.savePromptText, style: .default, handler: {
                alert -> Void in
                let locationNameTextField = locationAlertController.textFields![0] as UITextField
                self?.handleSaveNewLocationAction(control: locationNameTextField)
            })
            let cancelAction = UIAlertAction(title: SharedValues.MapController.cancelPromptText, style: .default)
            
            locationAlertController.addAction(cancelAction)
            locationAlertController.addAction(saveAction)
            self?.present(locationAlertController, animated: true, completion: nil)
            
            print("Save")
        }
        let useOnceAction = UIAlertAction(title: SharedValues.MapController.useOnlyPromptText, style: .default) { [weak self]_ in
            self?.handleUseOnceAction()
            print("Use Only")
        }
        let cancelAction = UIAlertAction(title: SharedValues.MapController.cancelPromptText, style: .cancel)
        
        actionSheetController.addAction(saveAction)
        actionSheetController.addAction(useOnceAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true, completion: nil)
    }
    
    private func handleUseOnceAction(){
        isSave = false
        buildFavourateLocationObject(name: "")
    }
    
    private func handleSaveNewLocationAction(control: UITextField){
        isSave = true
        if let name = self.buildValidLocationName(control: control) {
            buildFavourateLocationObject(name: name)
        }
    }
    
    private func buildValidLocationName(control: UITextField) -> String? {
        if let name = control.text {
            if !name.isEmpty && name.count > 0 {
                return name
            }
        }
        return nil
    }
    
    private func buildFavourateLocationObject(name: String) {
        getAdressName(name: name)
    }
    
    func getAdressName(name: String) {
        let location = CLLocation(latitude: mapKitView.centerCoordinate.latitude, longitude: mapKitView.centerCoordinate.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            var adressString: String = ""
            if error != nil {
                print("error getting Adddress")
                return
            } else {
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    let favourateLocation = FavourateLocation(name: name, addressName: adressString, latitude: self.mapKitView.centerCoordinate.latitude, longtitude: self.mapKitView.centerCoordinate.longitude)
                    self.handleCompletionAction(isSave: self.isSave, location: favourateLocation)
                    print(adressString)
                }
            }
        }
    }
    
    private func handleCompletionAction(isSave: Bool?, location: FavourateLocation){
        if isSave! {
            handleNavigationAfterSaveAction(location: location)
        } else {
            handleNavigationAfterUseOnceAction(location: location)
        }
    }
    
    private func handleNavigationAfterSaveAction(location: FavourateLocation){
        addFavouraateLocationToList(location: location)
        SharedObject.choosenDeliveryLocation = location
        navigationController?.popViewController(animated: true)
    }
    
    private func addFavouraateLocationToList(location: FavourateLocation){
        SharedObject.favourateLocationsList.append(location)
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: SharedObject.favourateLocationsList)
        UserDefaults.standard.set(encodedData, forKey: SharedValues.favourateLocationsListKey)
        UserDefaults.standard.synchronize()
    }
    
    private func handleNavigationAfterUseOnceAction(location: FavourateLocation){
        SharedObject.choosenDeliveryLocation = location
        navigationController?.popToRootViewController(animated: true)
    }
    
}

extension UINavigationController {
    var rootViewController : UIViewController? {
        return self.viewControllers.first as? ProviderViewController
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension MapController {
    
    fileprivate func setViewsConstraints(){
        addSubViewsInsideMainViews()
        setMapViewConstraints(map : mapKitView)
        setActionButtonConstraints(control: handleActionButton)
        setSelectedLocationConstraints(control: selectedLocationImage)
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(mapKitView)
        view.addSubview(handleActionButton)
        view.addSubview(myLocationButton)
        view.addSubview(selectedLocationImage)
        setMyLocationButton(control: myLocationButton)
    }
    
    private func setMyLocationButton(control : UIButton){
        NSLayoutConstraint.activate([
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            control.bottomAnchor.constraint(equalTo: handleActionButton.topAnchor, constant: -10),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight),
            control.widthAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setSelectedLocationConstraints(control : UIImageView){
        NSLayoutConstraint.activate([
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -10),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight),
            control.widthAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight)
            ])
    }
    
    private func setActionButtonConstraints(control : UIButton){
        let space: CGFloat
        if let tabBarHeight = tabBarController?.tabBar.frame.size.height {
            space = tabBarHeight
        } else {
            space = 50
        }
        NSLayoutConstraint.activate([
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight*1.5),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -space)
            ])
    }
    
    private func setMapViewConstraints(map : MKMapView){
        NSLayoutConstraint.activate([
            map.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            map.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            map.topAnchor.constraint(equalTo: view.topAnchor),
            map.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
    
}
