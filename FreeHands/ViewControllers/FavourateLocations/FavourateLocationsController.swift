//
//  FavourateLocationsController.swift
//  FreeHands
//
//  Created by Apple on 7/6/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class FavourateLocationsController: UICollectionViewController,  UICollectionViewDelegateFlowLayout {
    
    private var locations: [FavourateLocation] = [FavourateLocation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        self.collectionView!.register(LocationCell.self, forCellWithReuseIdentifier: SharedValues.FavourateLocation.locationCellId)
        collectionView?.register(LocationFooterCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: SharedValues.FavourateLocation.locationFooterCellId)
        collectionView?.backgroundView = UIImageView(image: SharedValues.ProviderController.backgroundImage)
        setupTapGesture()
        setupSwipeGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadCachedData()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .black
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SharedObject.favourateLocationsList.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(SharedValues.distanceBetweenControls, SharedValues.distanceBetweenControls, SharedValues.distanceBetweenControls, SharedValues.distanceBetweenControls)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.FavourateLocation.locationCellId, for: indexPath) as! LocationCell
        cell.currentObject = SharedObject.favourateLocationsList[indexPath.row]
        SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - SharedValues.distanceBetweenControls, height: SharedValues.defaultControlHeight*2)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: SharedValues.FavourateLocation.locationFooterCellId, for: indexPath) as! LocationFooterCell
        cell.cellButton.addTarget(self, action: #selector(footerTapped), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: SharedValues.defaultControlHeight*2)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension FavourateLocationsController {
    
    fileprivate func loadCachedData(){
        if let decoded  = UserDefaults.standard.object(forKey: SharedValues.favourateLocationsListKey) as? Data {
            SharedObject.favourateLocationsList = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [FavourateLocation]
        }
        collectionView?.reloadData()
    }
    
    @objc fileprivate func setupTapGesture(){
        let gesture = UITapGestureRecognizer(target: self, action: #selector(cellTapped))
        gesture.cancelsTouchesInView = false
        collectionView?.addGestureRecognizer(gesture)
    }
    
    @objc fileprivate func setupSwipeGesture(){
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(cellSwiped))
        gesture.cancelsTouchesInView = false
        collectionView?.addGestureRecognizer(gesture)
    }
    
    @objc fileprivate func cellSwiped(sender: UITapGestureRecognizer){
        if let indexPath = self.collectionView?.indexPathForItem(at: sender.location(in: self.collectionView)) {
            handleDeleteActionAt(index: indexPath.row)
        } else {
            print("collection view was swiped")
        }
    }
    
    @objc fileprivate func cellTapped(sender: UITapGestureRecognizer){
        if let indexPath = self.collectionView?.indexPathForItem(at: sender.location(in: self.collectionView)) {
            let cell = self.collectionView?.cellForItem(at: indexPath) as! LocationCell
            if let object = cell.currentObject {
                handleSelectionAction(location: object)
            }
        } else {
            print("collection view was tapped")
        }
    }
    
    fileprivate func handleDeleteActionAt(index: Int){
        let num = 1
        print("collection view was swipeddddddddd\(num+1)")
    }
    
    fileprivate func handleSelectionAction(location: FavourateLocation){
        SharedObject.choosenDeliveryLocation = location
        navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func footerTapped(sender: UITapGestureRecognizer){
        let controller = MapController()
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

