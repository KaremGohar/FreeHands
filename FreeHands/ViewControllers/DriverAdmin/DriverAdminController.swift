//
//  DriverAdminController.swift
//  FreeHands
//
//  Created by Apple on 7/18/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit
import DJWStarRatingView

class DriverAdminController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(patternImage: SharedValues.ProviderController.backgroundImage)
        setViewConstraits()
    }
    
    
    lazy var serviceCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(DriverServiceCell.self, forCellWithReuseIdentifier: SharedValues.DriverAdminController.driverAdminCellId)
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = .white
        return view
    }()
    
    let servicesHistoryTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        //        view.backgroundColor = .cyan
        view.textAlignment = .right
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 15)
        view.text = "Previous Services"
        return view
    }()
    
    let servicesButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.setBackgroundImage(UIImage(named: "plus_1"), for: .normal)
        view.addTarget(self, action: #selector(gotoServices), for: .touchUpInside)
        view.backgroundColor = .white
        return view
    }()
    
    let rating : DJWStarRatingView = {
        let view = DJWStarRatingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.starSize = CGSize(width: 20, height: 20)
        view.numberOfStars = 5
        view.fillColor = .black
        view.strokeColor = .black
        view.unfilledColor = .clear
        view.rating = 3.4
        return view
    }()
    
    let vehicleType: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        //        view.backgroundColor = .cyan
        view.textAlignment = .right
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 15)
        view.text = "Cairo, Egypt"
        return view
    }()
    
    let headerViewTitle: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        //        view.backgroundColor = .cyan
        view.textAlignment = .right
        view.textColor = SharedValues.appPreferredTextColor
        view.font = UIFont.systemFont(ofSize: 25)
        view.text = "Ahmed Haweel"
        return view
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //        view.backgroundColor = .red
        
        return view
    }()
    
    let personImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "person")
        view.contentMode = .scaleAspectFill
        return view
    }()

}

extension DriverAdminController {
    
    @objc fileprivate func gotoServices (){
        
    }
    
}

extension DriverAdminController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.DriverAdminController.driverAdminCellId, for: indexPath) as! DriverServiceCell
        SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: serviceCollectionView.frame.width, height: SharedValues.defaultControlHeight * 3)
    }
    
}

extension DriverAdminController {
    
    func setViewConstraits(){
        addSubViewsInsideMainViews()
        setHeaderViewConstraints(control: headerView)
        setTemplateImageView(image: personImageView)
        setHeaderViewTitleViewConstraints(control: headerViewTitle)
        setLocationTitleViewConstraints(control: vehicleType)
        setRatingViewConstraints(control: rating)
        setAddProductButtonViewConstraints(control: servicesButton)
        setProductCollectionViewConstraints(control: serviceCollectionView)
        setServicesHistoryTitleViewConstraints(control: servicesHistoryTitle)
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(personImageView)
        view.addSubview(headerView)
        view.addSubview(headerViewTitle)
        view.addSubview(vehicleType)
        view.addSubview(rating)
        view.addSubview(servicesButton)
        view.addSubview(serviceCollectionView)
        view.addSubview(servicesHistoryTitle)
    }
    
    private func setProductCollectionViewConstraints(control: UICollectionView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: servicesHistoryTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: SharedMethod.calculateTabBarHeight(tabController: tabBarController) * -1),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setServicesHistoryTitleViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: servicesButton.bottomAnchor),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 2),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3)
            ])
    }
    
    private func setAddProductButtonViewConstraints(control: UIButton){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 1),
            control.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6)
            ])
    }
    
    private func setRatingViewConstraints(control: DJWStarRatingView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: vehicleType.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.15),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.3)
            ])
    }
    
    private func setLocationTitleViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerViewTitle.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.15),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.4)
            ])
    }
    
    private func setHeaderViewTitleViewConstraints(control: UILabel){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: headerView.topAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.2),
            control.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.6)
            ])
    }
    
    private func setHeaderViewConstraints(control: UIView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: personImageView.bottomAnchor, constant: SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 3),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setTemplateImageView(image: UIImageView){
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor, constant: SharedMethod.calculateNavigationBarHeight(navigationController: navigationController)),
            image.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 5),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
    }
    
}
