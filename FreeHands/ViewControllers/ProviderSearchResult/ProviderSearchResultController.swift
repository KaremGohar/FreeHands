//
//  ProviderSearchResultController.swift
//  FreeHands
//
//  Created by Apple on 7/10/18.
//  Copyright © 2018 syntaxerror. All rights reserved.
//

import UIKit

class ProviderSearchResultController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundView = UIImageView(image: SharedValues.ProviderController.backgroundImage)
        view.backgroundColor = UIColor(patternImage: SharedValues.ProviderController.backgroundImage)
        setViewsConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationControllerItems()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SharedValues.ProviderSearchResultController.providerCellId, for: indexPath) as! ProviderCell
        cell.providerImage.image = UIImage(named: "222")
        SharedMethod.addShadowAndCornerRadiasTo(cell: cell, cornerRadias: 10, borderWidth: 2, borderColor: UIColor.clear.cgColor, shadowColor: UIColor.gray.cgColor, shadowOffsetWidth: 3, shadowOffsetHeight: 5, shadowRadius: 10)
        return cell
    }
    
    lazy var providersCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.register(ProviderCell.self, forCellWithReuseIdentifier: SharedValues.ProviderSearchResultController.providerCellId)
        view.backgroundColor = .clear
        return view
    }()
    
    let AdImageView : UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = SharedValues.ProviderSearchResultController.adDefaultImage
        view.contentMode = .scaleAspectFill
//        view.backgroundColor = .red
        return view
    }()
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Events Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension ProviderSearchResultController {
    
    private func setupRightNavItems(){
        let searchButton = UIButton(type: .system)
        searchButton.setImage(#imageLiteral(resourceName: "search"), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        let actionSheetButton = UIButton(type: .system)
        actionSheetButton.setImage(#imageLiteral(resourceName: "circles"), for: .normal)
        actionSheetButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: actionSheetButton), UIBarButtonItem(customView: searchButton)]
    }
    
    fileprivate func setupNavigationControllerItems(){
        navigationItem.title = SharedObject.choosenDeliveryLocation?.addressName
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .black
    }
    
}

extension ProviderSearchResultController {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.pushViewController(ProviderProductController(), animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding, SharedValues.defaultPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: SharedValues.defaultControlHeight*3)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// View Extension ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension ProviderSearchResultController {
    
    fileprivate func setViewsConstraints() {
        addSubViewsInsideMainViews()
        setAdImageViewConstraints(control: AdImageView)
        setproviderCollectionViewConstraints(control: providersCollectionView)
        setupRightNavItems()
    }
    
    private func addSubViewsInsideMainViews(){
        view.addSubview(AdImageView)
        view.addSubview(providersCollectionView)
    }
    
    private func setproviderCollectionViewConstraints(control: UICollectionView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: AdImageView.bottomAnchor, constant: SharedValues.defaultPadding * 2),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: SharedValues.defaultPadding * -1),
            control.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: SharedValues.defaultPadding)
            ])
    }
    
    private func setAdImageViewConstraints(control: UIImageView){
        NSLayoutConstraint.activate([
            control.topAnchor.constraint(equalTo: view.topAnchor, constant: SharedMethod.calculateNavigationBarHeight(navigationController: self.navigationController) + SharedValues.defaultPadding),
            control.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            control.heightAnchor.constraint(equalToConstant: SharedValues.defaultControlHeight * 4),
            control.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
}



